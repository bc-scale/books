from rest_framework import serializers

from books import models


class PublisherSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Publisher
        fields = "__all__"


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Book
        fields = "__all__"


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Author
        fields = "__all__"
