from django.db import models


class Publisher(models.Model):
    name = models.CharField(max_length=200)
    website = models.URLField()
    country = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=200)
    publisher = models.ForeignKey(
        Publisher, related_name="books", on_delete=models.CASCADE
    )
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.title


class Author(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    books = models.ManyToManyField(Book, related_name="authors")

    def __str__(self):
        return self.first_name + " " + self.last_name
