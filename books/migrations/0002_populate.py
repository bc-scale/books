from django.db import migrations


def populate_publishers(apps, schema_editor):

    print("========== populate publishers")
    Data = apps.get_model("books", "Publisher")
    records = [
        {
            "name": "Mondadori",
            "website": "https://www.mondadori.it",
            "country": "Italy",
        },
        {
            "name": "Giulio Einaudi",
            "website": "https://www.einaudi.it",
            "country": "Italy",
        },
        {
            "name": "La nave di Teseo",
            "website": "http://www.lanavediteseo.eu",
            "country": "Italy",
        },
        {
            "name": "Gallimard",
            "website": "https://www.gallimard.fr",
            "country": "France",
        },
        {
            "name": "Penguin Books",
            "website": "https://www.penguin.co.uk",
            "country": "UK",
        },
    ]
    for record in records:
        obj = Data.objects.create(**record)
        print("============ created: ", obj)


def populate_authors(apps, schema_editor):

    print("========== populate authors")
    Data = apps.get_model("books", "Author")
    records = [
        {"first_name": "Gianni", "last_name": "Barbacetto"},
        {"first_name": "Peter", "last_name": "Gomez"},
        {"first_name": "Marco", "last_name": "Travaglio"},
        {"first_name": "Marco", "last_name": "Pisellonio"},
        {"first_name": "Kate", "last_name": "Rowling"},
        {"first_name": "André", "last_name": "Prevost"},
        {"first_name": "Michail", "last_name": "Gorbatchev"},
    ]
    for record in records:
        obj = Data.objects.create(**record)
        print("============ created: ", obj)


def populate_books(apps, schema_editor):

    print("========== populate books")
    Data = apps.get_model("books", "Book")

    Publisher = apps.get_model("books", "Publisher")
    p1 = Publisher.objects.get(pk=1)
    p2 = Publisher.objects.get(pk=2)
    p3 = Publisher.objects.get(pk=3)
    p4 = Publisher.objects.get(pk=4)
    p5 = Publisher.objects.get(pk=5)

    Author = apps.get_model("books", "Author")
    a1 = Author.objects.get(pk=1)
    a2 = Author.objects.get(pk=2)
    a3 = Author.objects.get(pk=3)
    a4 = Author.objects.get(pk=4)
    a5 = Author.objects.get(pk=4)
    a6 = Author.objects.get(pk=4)
    a7 = Author.objects.get(pk=4)

    records = [
        {
            "title": "Il giro del mondo in 80 giorni",
            "publisher": p1,
            "price": 10.99,
            "authors": [a1, a2],
        },
        {
            "title": "Pinguini di tutto il mondo",
            "publisher": p2,
            "price": 12.50,
            "authors": [a1, a3, a4],
        },
        {
            "title": "La vie clandestine",
            "publisher": p4,
            "price": 12.50,
            "authors": [a6],
        },
        {
            "title": "Il milione",
            "publisher": p5,
            "price": 22.90,
            "authors": [a3],
        },
        {
            "title": "The Guinnes records",
            "publisher": p3,
            "price": 15.50,
            "authors": [a2],
        },
        {
            "title": "La vie en rose",
            "publisher": p2,
            "price": 21.50,
            "authors": [a6],
        },
        {
            "title": "Business models",
            "publisher": p1,
            "price": 8.49,
            "authors": [a3, a6],
        },
        {
            "title": "La regola d'oro",
            "publisher": p5,
            "price": 5.50,
            "authors": [a1, a4],
        },
        {
            "title": "The lord of the rings",
            "publisher": p3,
            "price": 11.00,
            "authors": [a2, a5, a4],
        },
        {
            "title": "Quelque chose à te dire",
            "publisher": p2,
            "price": 10.00,
            "authors": [a5, a7],
        },
    ]
    for record in records:
        authors = record.pop("authors")
        obj = Data.objects.create(**record)
        for author in authors:
            obj.authors.add(author)
        print("============ created: ", obj)


class Migration(migrations.Migration):

    dependencies = [
        ("books", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(populate_publishers),
        migrations.RunPython(populate_authors),
        migrations.RunPython(populate_books),
    ]
